<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('admin.index');
});


Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function(){
        Route::get('/',                                     'Admin\HomeController@index')->name('admin');
        Route::get('/tamu/daftar-tamu',                     'Admin\GuestController@index')->name('admin.guest');
        Route::get('/tamu/tambah-tamu',                     'Admin\GuestController@add')->name('admin.guest.add');
        Route::post('/tamu/tambah-tamu/insert',             'Admin\GuestController@post_tamu')->name('admin.guest.insert');
        Route::get('/tamu/tambah-tamu/hapus/{id}',          'Admin\GuestController@hapus')->name('admin.guest.delete');
        Route::get('/tamu/tambah-tamu/update/{id}',         'Admin\GuestController@update')->name('admin.guest.update');

});

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

<?php

use Illuminate\Database\Seeder;
Use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = new User;
            $data->name          = 'Fadhli Munadi';
            $data->email         = 'fadhli@galaxidata.com';
            $data->id_role       = 1;
            $data->foto          = 'fadhli.jpg';
            $data->username      = 'fadhli';
            $data->jabatan       = 'Admin Buku Tamu';
            $data->password      = bcrypt('123456');
            $data->save();
    }
}

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Lapor Minahasa" />
    <meta name="author" content="Fadhli Developer" />
    <title>Login Buku Tamu PT Galaxi Data</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="{{('backend/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="{{('backend/assets/plugins/iconic/css/material-design-iconic-font.min.css')}}">
    <!-- bootstrap -->
	<link href="{{('backend/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="{{('backend/assets/css/pages/extra_pages.css')}}">
	<!-- favicon -->
    <link rel="shortcut icon" href="{{('backend/assets/img/bg.ico')}}" />
</head>
<body>
    <div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
        <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
					<span class="login100-form-logo">
              <img src="{{url('backend/assets/img/bg.jpg')}}" alt="Logo PT GALAXI" />
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>
					<div class="wrap-input100" data-validate = "Enter Email / Username">
						<input class="input100" type="text" name="email" placeholder="Enter Email / Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>

            @if ($errors->has('email'))
                <span class="help-blocks">
                    <strong >{{ $errors->first('email') }}</strong>
                </span>
            @endif
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>

            @if ($errors->has('password'))
                <span class="help-blocks">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
					</div>
					<div class="contact100-form-checkbox">
              <input type="checkbox" class="input-checkbox100" id="ckb1" name="remember" {{ old('remember') ? 'checked' : '' }}>
						 	<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
    <!-- start js include path -->
    <script src="{{('backend/assets/plugins/jquery/jquery.min.js')}}" ></script>
    <!-- bootstrap -->
    <script src="{{('backend/assets/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
    <script src="{{('backend/assets/js/pages/extra_pages/login.js')}}" ></script>
    <!-- end js include path -->
</body>
</html>

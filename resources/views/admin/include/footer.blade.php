<!-- start footer -->
<div class="page-footer">
    <div class="page-footer-inner"> 2018 &copy; Buku Tamu
        <a href="mailto:fadhlimunadi@gmail.com" target="_top" class="makerCss">Fadhli Munadi Iman</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- end footer -->

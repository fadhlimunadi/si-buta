
<div class="sidebar-container">
  <div class="sidemenu-container navbar-collapse collapse fixed-menu">
            <div id="remove-scroll">
                <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <li class="sidebar-toggler-wrapper hide">
                        <div class="sidebar-toggler">
                            <span></span>
                        </div>
                    </li>
                    <li class="sidebar-user-panel">
                        <div class="user-panel">
                            <div class="row">
                                      <div class="sidebar-userpic">
                                          <img src="{{url('backend/assets/img')}}/{{ Auth::user()->foto }}" class="img-responsive" alt=""> </div>
                                  </div>
                                  <div class="profile-usertitle">
                                      <div class="sidebar-userpic-name">{{ Auth::user()->name }}</div>
                                      <div class="profile-usertitle-job">{{ Auth::user()->jabatan }}</div>
                                  </div>
                                  <div class="sidebar-userpic-btn">
                    <a class="tooltips" href="#" data-placement="top" data-original-title="Profile">
                      <i class="material-icons">person_outline</i>
                    </a>

                    <a role="menuitem" tabindex="-1" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons">input</i>  </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                </div>
                        </div>
                    </li>

                    <li class="nav-item start {{active('admin')}}  ">
                        <a href="{{route('admin')}}" class="nav-link nav-toggle">
                            <i class="material-icons">dashboard</i>
                            <span class="title">Dashboard</span>
                        </a>

                    </li>

                    <li class="nav-item {{active('admin.guest')}} ">
                        <a href=" " class="nav-link nav-toggle"> <i class="material-icons">account_circle</i>
                            <span class="title">Daftar Tamu</span> <span class="arrow"></span>
                        </a>

                        <ul class="sub-menu">
                            <li class="nav-item {{active('admin.guest.add')}}">
                                <a href="{{route('admin.guest.add')}}" class="nav-link "> <span class="title">Tulis Tamu Baru</span>
                                </a>
                            </li>

                            <li class="nav-item {{active('admin.guest')}}">
                                <a href="{{route('admin.guest')}}" class="nav-link "> <span class="title">Data Tamu</span>
                                </a>
                            </li>

                        </ul>
                    </li>


                </ul>
            </div>
          </div>
      </div>

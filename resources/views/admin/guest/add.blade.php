@extends ('admin.layouts.app')
@section('title', 'Tulis Tamu Baru')
<link href="{{asset('backend/assets/plugins/summernote/summernote.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('backend/assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css')}}" />
<link href="{{asset('backend/assets/plugins/jquery-tags-input/jquery-tags-input.css')}}" rel="stylesheet">
  <link href="{{asset('backend/assets/plugins/dropzone/dropzone.css')}}" rel="stylesheet" media="screen">

@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
      <div class="row p-b-20">
        @if(session()->has('success'))
           <div class="alert alert-success">
               <strong>Berhasil!</strong> {{ session()->get('success') }}
           </div>
       @elseif ($errors->any())
         <div class="alert alert-danger">
             <ul>
         @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
         @endforeach
             </ul>
         </div>
      @endif
      </div>
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Tulis Tamu Baru</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Dashboard</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="">Daftar Tamu </a>&nbsp; <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a class="parent-item" href="">Tulis Tamu Baru </a>&nbsp;
                    </li>
                </ol>
            </div>
        </div>
          <!-- basic wizard -->


        <!-- wizard with validation-->
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">
              <div class="card-head">
                <header>Form Tamu Baru</header>
              </div>
              <form method="post" action="{{route('admin.guest.insert')}}" enctype="multipart/form-data">
                {{csrf_field () }}
              <div class="card-body row">

                    <div class="col-lg-6 p-t-20">
                        <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-width">
                             <input class = "mdl-textfield__input" type = "text" id = "text4" name="nama">
                             <label class = "mdl-textfield__label" for = "text4">Nama Tamu</label>
                          </div>
                      </div>

                      <div class="col-lg-6 p-t-20">
                          <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-width">
                               <input class = "mdl-textfield__input" type = "number" id = "number" name="no_telpon">
                               <label class = "mdl-textfield__label" for = "text4">Nomor Telfon</label>
                            </div>
                        </div>

                        <div class="col-lg-6 p-t-20">
                            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-width">
                                 <input class = "mdl-textfield__input" type = "text" id = "kegiatan" name="kegiatan">
                                 <label class = "mdl-textfield__label" for = "text4">Kegiatan</label>
                              </div>
                          </div>

                          <div class="col-lg-6 p-t-20">
                              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-width">
                                   <input class = "mdl-textfield__input" type = "number" id = "number" name="jumlah_orang">
                                   <label class = "mdl-textfield__label" for = "text4">Jumlah Orang</label>
                                </div>
                            </div>

                      <div class="col-lg-12 p-t-20">
                          <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    Submit
                  </button>
                      </div>
                    </form>
              </div>
            </div>
          </div>
        </div>
        </div>
        <!-- Verticle Steps Wizard -->

    </div>
</div>
@endsection
@section('js')
<script src="{{asset('backend/assets/plugins/jquery/jquery.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/popper/popper.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/jquery-blockui/jquery.blockui.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('backend/assets/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
<!-- Common js-->
<script src="{{asset('backend/assets/js/app.js')}}" ></script>
<script src="{{asset('backend/assets/js/layout.js')}}" ></script>
<script src="{{asset('backend/assets/js/theme-color.js')}}" ></script>
<!-- Material -->
<script src="{{asset('backend/assets/plugins/material/material.min.js')}}"></script>
<!-- animation -->
<script src="{{asset('backend/assets/js/pages/ui/animations.js')}}" ></script>
<!-- summernote -->
@endsection

@extends ('admin.layouts.app')
@section('title', 'Data Tamu')
<link href="{{asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"/>
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Daftar Tamu</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Dashboard</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="">Daftar Tamu</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                </ol>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Tamu</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                          <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                          <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-12 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                </div>
                            </div>
                        </div>
                        <div class="table-scrollable">
                        <table class="table table-hover table-checkable order-column full-width" id="example4">
                            <thead>
                                <tr>
                                  <th class="center">Nama</th>
                                    <th class="center">No. Telfon</th>
                                    <th class="center">Kegiatan</th>
                                    <th class="center">Jumlah Orang</th>
                                    <th class="center">Masuk</th>
                                    <th class="center">Keluar</th>
                                    <th class="center">Status</th>
                                    <th class="center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                  @foreach ($tamu as $t)
                           <tr class="odd gradeX">
                             <td class="center">{{$t->nama}}</td>
                             <td class="center">{{$t->no_telpon}}</td>
                             <td class="center">{{$t->kegiatan}}</td>
                             <td class="center">{{$t->jumlah_orang}}</td>
                             <td class="center">{{ $t->created_at }}</td>
                             @if($t->status == 'On Going')
                             <td class="center"> </td>
                             <td class="center"><span class="label label-sm label-info">On Going</span></td>
                              @else
                             <td class="center">{{ $t->created_at }}</td>
                             <td class="center"><span class="label label-sm label-success">Sudah Selesai</span></td>
                             @endif
                             <td class=""><div class=" btn-group btn-group-solid">
                             <a href="{{route('admin.guest.update', $t->id)}}" class="btn btn-warning">Tandai Sudah Keluar</a>
                             <a href="{{route('admin.guest.delete', $t->id)}}" class="btn btn-danger">Hapus</a>
                           </div></td>
                          </tr>
                @endforeach
                          </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('backend/assets/plugins/jquery/jquery.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/popper/popper.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/jquery-blockui/jquery.blockui.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('backend/assets/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
<!-- Common js-->
<script src="{{asset('backend/assets/js/app.js')}}" ></script>
<script src="{{asset('backend/assets/js/layout.js')}}" ></script>
<script src="{{asset('backend/assets/js/theme-color.js')}}" ></script>
<!-- data tables -->
<script src="{{asset('backend/assets/plugins/datatables/jquery.dataTables.min.js')}}" ></script>
<script src="{{asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js')}}" ></script>
<script src="{{asset('backend/assets/js/pages/table/table_data.js')}}" ></script>
<!-- Material -->
<script src="{{asset('backend/assets/plugins/material/material.min.js')}}"></script>
<!-- animation -->
<script src="{{asset('backend/assets/js/pages/ui/animations.js')}}" ></script>
@endsection

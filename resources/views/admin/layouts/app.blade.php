
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <title>@yield('title')</title>
  @include('admin.include.head')
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
		@include('admin.include.header')
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			@include('admin.include.sidebar')
			 <!-- end sidebar menu -->
			<!-- start page content -->
            @yield('content')
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        @include('admin.include.footer')
        <!-- end footer -->
    </div>
    <!-- start js include path -->

@yield('js')
    <!-- end js include path -->
</body>
</html>

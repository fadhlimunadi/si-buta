<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tamu extends Model
{

  protected $table = 'tamu';
  protected $fillable = [
      'nama', 'kegiatan', 'jumlah_orang','no_telpon','status'
  ];
}

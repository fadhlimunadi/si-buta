<?php

namespace App\Http\Controllers\Admin;

use App\Model\Tamu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class GuestController extends Controller
{
  public function index() {
    $tamu = Tamu::all();
    return view('admin.guest.index', compact('tamu'));
  }

  public function add() {
    return view('admin.guest.add');
  }
  public function post_tamu(Request $req) {

      $this->validate($req, [
        'nama' => 'required',
        'kegiatan' => 'required',
        'no_telpon' => 'required',
        'jumlah_orang' => 'required',
        ]);

        $insert_post= Tamu::create([
        'nama'      => $req->nama,
        'kegiatan'    => $req->kegiatan,
        'no_telpon'  => $req->no_telpon,
        'jumlah_orang'  => $req->jumlah_orang,
      ]);

      return redirect()
      ->route('admin.guest')
      ->with('success', 'Anda sudah menambahkan Data Tamu');
      }

  public function update($id) {


    $updt = DB::table('tamu')
    ->where('id', $id)
    ->update(['status' => "Sudah Selesai"]);


    return redirect()
    ->back()
    ->with('success', 'Anda sudah menandai jadwal pulang.');

  }


    public function hapus($id){

      DB::table('tamu')
      ->where('id', $id)->delete();

      return redirect()
      ->back()
      ->with('success', 'Anda menghapus data tamu');
    }
}

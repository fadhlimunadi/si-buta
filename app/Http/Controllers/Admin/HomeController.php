<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Tamu;

class HomeController extends Controller
{
  public function index() {
    $tamu        = Tamu::where('status', '=', 'On Going')->count();
    $tamu_finish = Tamu::where('status', '=', 'Sudah Selesai')->count();
    $tamu_all    = Tamu::where('status', '=', 'On Going')->get();
    return view('admin.home.index', compact('tamu','tamu_finish','tamu_all'));
  }
}
